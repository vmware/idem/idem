---
stages:
  - pre-commit
  - test
  - pkg
  - pkg-tests
  - pop-release
  - publish

include:
  - project: saltstack/pop/cicd/ci-templates
    file: /lint/pre-commit-run-all-py39.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/build-docs-html-nox.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /release/pop_release.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docker/kaniko-single-combined.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-gitlab.yml
  - project: saltstack/pop/cicd/ci-templates
    file: /docswork/publish-docs-release.yml

workflow:
  rules:
    - if: $CI_MERGE_REQUEST_ID == null

variables:
  CICD_UPSTREAM_PATH: "vmware/idem/idem"

.test-suite:
  variables:
    ACCT_FILE: .gitlab-ci-creds.yml
    ACCT_KEY:
  stage: test
  needs:
    - pre-commit-run-all
  script:
    - python3 -m pip install nox
    - nox -e tests-3 -- -vv
  services:
    - name: rabbitmq:management
      variables:
        RABBITMQ_HOSTS: rabbitmq
        RABBITMQ_PORT: 5672
        RABBITMQ_USER: guest
        RABBITMQ_PASS: guest
        RABBITMQ_PROTOCOL: amqp
    - name: bitnami/kafka:latest
      alias: kafka
      variables:
        KAFKA_ENABLE_KRAFT: 'yes'
        KAFKA_CFG_PROCESS_ROLES: 'broker,controller'
        KAFKA_CFG_CONTROLLER_LISTENER_NAMES: 'CONTROLLER'
        KAFKA_CFG_LISTENER_SECURITY_PROTOCOL_MAP: 'CLIENT:PLAINTEXT,INTERNAL:PLAINTEXT'
        KAFKA_CFG_LISTENERS: 'CLIENT://:9092,INTERNAL://:9093'
        KAFKA_CFG_ADVERTISED_LISTENERS: 'CLIENT://kafka:9092,INTERNAL://localhost:9093'
        KAFKA_CFG_INTER_BROKER_LISTENER_NAME: 'INTERNAL'
        KAFKA_BROKER_ID: '1'
        KAFKA_CFG_CONTROLLER_QUORUM_VOTERS: '1@127.0.0.1:9093'
        ALLOW_PLAINTEXT_LISTENER: 'yes'
  artifacts:
    when: always
    paths:
      - artifacts
    reports:
      junit: artifacts/junit-report.xml
    expire_in: 30 days

tests-3.8:
  extends: .test-suite
  image: python:3.8

tests-3.9:
  extends: .test-suite
  image: python:3.9

tests-3.9-macos1015:
  extends: .test-suite
  tags:
    - vmw-macos-catalina
  rules:
    - if: "$CI_PROJECT_PATH == $CICD_UPSTREAM_PATH"
      when: on_success

#tests-3.9-windows1021H2:
#  extends: .test-suite
#  tags:
#    - shared-windows
#  script:
#    - choco install python --version=3.9.4 -y -f
#    - C:\\Python39\\python.exe -m pip install nox acct
#    - C:\\Python39\\Scripts\\acct.exe encrypt .gitlab-ci-creds.yml --acct-key="$ACCT_KEY"
#    - C:\\Python39\\Scripts\\nox.exe -e tests-3 -- -vv

tests-3.10:
  extends: .test-suite
  image: python:3.10.0

tests-3.11:
  extends: .test-suite
  image: python:3.11.0

idem-cloud-tests:
  stage: pkg-tests
  variables:
    CICD_SOURCE_PROJECT_NAME: $CI_PROJECT_NAME
    CICD_SOURCE_COMMIT: $CI_COMMIT_SHA
    CICD_SOURCE_GITLAB_PROJECT_PATH: $CI_PROJECT_PATH
  trigger:
    project: vmware/idem/cicd/idem-cloud-test
    strategy: depend
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED == "true" && $CI_PROJECT_PATH == $CICD_UPSTREAM_PATH'
      when: on_success
    - when: never

# Docs jobs using templeats
build-docs-html:
  stage: test

pages:
  stage: pkg
  variables:
    CICD_DOCS_VERSION_LATEST: latest

publish-docs:
  stage: pop-release
  variables:
    CICD_S3_UPLOAD_DOCS: "true"
    CICD_S3_SOURCE_PATH: "public/"
    CICD_S3_DEST_PATH: "docs.idemproject.io/idem/"

dockerhub-release:
  stage: publish
  dependencies: []
  extends: .kaniko-build
  variables:
    CONTAINER_NAME: idem
    CONTAINER_TAG: latest
    CONTEXT_PATH: ./
    DOCKERFILE_PATH: docker/idem.Dockerfile
    KANIKO_DOCKERHUB_NAMESPACE: saltstack
