def __init__(hub):
    hub.exec.test_resource.ACCT = ["test"]
    hub.exec.test_resource.INC = 0


def get(
    hub,
    *,
    resource_id: str,
    recreate_resource_without_resource_id: str = False,
    **kwargs
):
    """
    A get call that uses resource_id as a resource_id, the right way
    """
    if recreate_resource_without_resource_id:
        return {
            "result": True,
            "comment": "get",
        }
    return {
        "result": True,
        "comment": "get",
        "ret": dict(resource_id=resource_id, inc=hub.exec.test_resource.INC, **kwargs),
    }


def action(hub, *, resource_id: str, **kwargs):
    """
    A get call that uses resource_id as a resource_id, the right way
    """
    hub.exec.test_resource.INC += 1

    return {
        "result": True,
        "comment": "get",
        "ret": dict(resource_id=resource_id, inc=hub.exec.test_resource.INC, **kwargs),
    }


def name(hub, *, name: str, **kwargs):
    """
    A get call that will use "name" as the resource_id
    """
    return {
        "result": True,
        "comment": "name",
        "ret": dict(resource_id=name, inc=hub.exec.test_resource.INC, **kwargs),
    }


def positional(hub, positional: str, **kwargs):
    """
    A get call that will use the first positional argument as the resource_id
    """
    return {
        "result": True,
        "comment": "positional",
        "ret": dict(resource_id=positional, inc=hub.exec.test_resource.INC, **kwargs),
    }


def none(hub):
    """
    A get call which takes no parameters
    """
    return {
        "result": True,
        "comment": "none",
        "ret": dict(resource_id=None, inc=hub.exec.test_resource.INC),
    }


def empty(hub):
    """
    A get call which takes no parameters
    """
    return {
        "result": True,
        "comment": "empty",
        "ret": {},
    }
