from pytest_idem.runner import run_yaml_block

STATE = """
state:
  test.present:
    - comment: {comment}
"""


def test_true():
    ret = run_yaml_block(STATE.format(comment=True))
    state_ret = next(iter(ret.values()))
    assert state_ret["comment"] == [True]


def test_false():
    ret = run_yaml_block(STATE.format(comment=False))
    state_ret = next(iter(ret.values()))
    assert state_ret["comment"] == [False]


def test_none():
    ret = run_yaml_block(STATE.format(comment="null"))
    state_ret = next(iter(ret.values()))
    assert state_ret["comment"] == []


def test_string():
    ret = run_yaml_block(STATE.format(comment="str"))
    state_ret = next(iter(ret.values()))
    assert state_ret["comment"] == ["str"]


def test_bytes(hub):
    state_ret = hub.states.test.present({}, "", comment=b"bytes")
    assert state_ret["comment"] == [b"bytes"]


def test_tuple(hub):
    state_ret = hub.states.test.present({}, "", comment=("tuple",))
    assert state_ret["comment"] == ["tuple"]


def test_flatten():
    ret = run_yaml_block(
        STATE.format(
            comment=["a", "b", ["c", ["d", ["e"], "f"], "g"], "h", "i", [[[["j"]]]]]
        )
    )
    state_ret = next(iter(ret.values()))
    assert state_ret["comment"] == ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j"]
