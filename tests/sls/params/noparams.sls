vpc:
  test.present:
    - new_state:
        resource_id: {{ params.get("vpc_resource_id") }}

vpc1:
  test.present:
    - new_state:
        resource_id: {{ params.get("vpc_resource_id1") }}

vp2:
  test.present:
    - new_state:
        resource_id: {{ params.get("vpc_resource_id2") }}
