happy:
  test.nop:
  - require:
    - test: indifferent

sad:
  test.nop:
  - require:
    - test: happy

indifferent:
  test.nop:
  - require:
    - test: sad
