# green field scenario: when create_before_destroy is false & resource_id is None

create_scenario:
  test.present:
    - new_state:
        key: value
        name: test
        resource_id: idem-test-1
    - result: true
    - recreate_on_update:
        create_before_destroy: false
