dep-1:
  test.unique_op:
    - require:
      - test: ind-1

dep-2:
  test.unique_op:
    - require:
      - test: dep-1

dep-3:
  test.unique_op:
    - require:
      - test: dep-2

ind-1:
  test.unique_op

ind-2:
  test.unique_op
