"""Demo Idem Provider plugin for interacting with an sqlite database."""
import os
import sqlite3

__func_alias__ = {"list_": "list"}


async def update(
    hub, ctx, resource_id, name=None, age=None, db_path="~/idem-sqlite-demo.db"
):
    """Update a database entry.

    Args:
        resource_id(int):
            The person's unique id

        name (str):
            The person's name.

        age (int):
            The person's age.

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.update name="Sumit" age=35 resource_id=3

        .. code-block:: bash

            $ idem exec demo.sqlite.update  resource_id=5 age=39
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    cur = con.cursor()

    # Get existing values
    val = await hub.exec.demo.sqlite.get(ctx, resource_id=resource_id)
    vals = val["ret"]

    # Update existing values with new values, if any are supplied
    if name:
        vals["name"] = name
    if age:
        vals["age"] = age
    cur.execute(
        "UPDATE people SET name=?, age=? WHERE resource_id=?",
        (vals["name"], vals["age"], resource_id),
    )
    con.commit()

    # Let's query our database to ensure our data is there.
    val = await hub.exec.demo.sqlite.get(ctx, resource_id=resource_id)
    result["ret"] = val["ret"]
    con.close()
    return result


async def delete(hub, ctx, resource_id, db_path="~/idem-sqlite-demo.db"):
    """Delete a specific entry in our database.

    Args:
        resource_id(str):
            The resource_id of the database entry.

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.delete 2343

    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    val = cur.execute(
        "Delete FROM people WHERE resource_id = ?",
        (resource_id,),
    )
    con.commit()
    result["ret"] = f"{cur.rowcount} row(s) deleted"
    con.close()
    return result


async def create(hub, ctx, name, age, db_path="~/idem-sqlite-demo.db"):
    """Create a new database entry.

    Args:
        name (str):
            The person's name.

        age (int):
            The person's age.

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.create name="Sumit" age=35

        .. code-block:: bash

            $ idem exec demo.sqlite.create Sarah 36
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    cur = con.cursor()
    cur.execute("INSERT INTO people VALUES(null, ?, ?)", (name, age))
    con.commit()

    # Let's query our database to ensure our data is there.
    val = await hub.exec.demo.sqlite.get(ctx, resource_id=cur.lastrowid)
    result["ret"] = val["ret"]
    con.close()
    return result


async def get(hub, ctx, resource_id, db_path="~/idem-sqlite-demo.db"):
    """Retrieve information for a specific entry in our database.

    Args:
        resource_id(str):
            The resource_id of the database entry.

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.get 2343

    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    val = cur.execute(
        "SELECT resource_id, name, age FROM people WHERE resource_id = ?",
        (resource_id,),
    ).fetchone()
    if val:
        result["ret"] = dict(val)  # convert a valid response to a dictionary
    con.close()
    return result


async def list_(hub, ctx, db_path="~/idem-sqlite-demo.db"):
    """List all people database entries.

    Args:

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.list
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    vals = cur.execute("SELECT resource_id, name, age FROM people").fetchall()
    if vals:
        result["ret"] = await hub.tool.demo.sqlite.sqlite_to_dict(ctx, vals)
    con.close()
    return result


async def setup_db(hub, ctx, db_path="~/idem-sqlite-demo.db"):
    """Create a new sqlite database.

    Args:

        db_path (str, Optional):
            The path to the desired location of the sqlite file.

    Examples:
        Calling from the CLI:

        .. code-block:: bash

            $ idem exec demo.sqlite.setup_db
    """
    result = dict(comment=[], ret=None, result=True)
    con = sqlite3.connect(os.path.expanduser(db_path))
    con.row_factory = sqlite3.Row
    cur = con.cursor()

    # check if table already exists
    tables = cur.execute(
        """SELECT name FROM sqlite_master WHERE type='table'
        AND name='people'; """
    ).fetchall()
    if not tables:
        cur.execute(
            """CREATE TABLE people
                (resource_id integer primary key AUTOINCREMENT,
                name varchar(20) NOT NULL,
                age varchar(20) NOT NULL)"""
        )
        cur.execute(
            """
            INSERT INTO people VALUES
                (null, 'Sarah', 28),
                (null, 'David', 45)
                """
        )
        con.commit()
    vals = cur.execute("SELECT resource_id, name, age FROM people").fetchall()
    if vals:
        result["ret"] = await hub.tool.demo.sqlite.sqlite_to_dict(ctx, vals)
    con.close()
    return result
