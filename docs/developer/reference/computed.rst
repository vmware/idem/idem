===================
Computed Parameters
===================

Computed parameters are used to mark function arguments as computed or read-only.
These parameters are not defined in the SLS file, but rather returned when the resource operation is executed.
Computed parameters can be used as input to other resources in the SLS using argument binding.

.. code-block:: python

    async def present(hub, ctx, name: str, zone: str, ip_address: Computed[str] = None):
        ...

Computed parameters are skipped when generating resource changes.
