==================================
Managing Idem projects with Poetry
==================================

Poetry is a tool for dependency management and packaging in Python. It is the recommended tool for managing your Idem cloud provider plugins for your Idem cloud infrastructure code.

There are several benefits to using Poetry for package management.

Clear package management
========================

Because the packages you add using `poetry add` and their version requirements are recorded to `pyproject.toml`, you can see the exact plugins or packages you have explicitly installed and the version requirements you have set for them.

Environment reproducibility
===========================

When you add a plugin or package, Poetry records the exact package version and versions of any installed dependencies to `poetry.lock`, including hashes. Running `poetry install –sync` installs the exact set of packages recorded in `poetry.lock` and removes any other packages. This makes your environment reproducible across machines.

Integration with infrastructure configuration
=============================================

Because you add `poetry.lock` and `pyproject.toml` to your SCM, the package versions used in your Python environment are tied to your infrastructure configuration files. When combined with a post-checkout hook, your environment can automatically update to the exact plugin configuration used at the time that infrastructure configuration was written.

Clear package upgrades
======================

When you upgrade a package with pip, you can end up with incompatible packages installed simultaneously.

For example, imagine that you installed idem==20.0.0, then installed idem-aws==1.4.0, and then installed idem==23.0.0. After installation, you'll see an error in the output, but the command will report a successful install. However, idem==23.0.0 and idem-aws==1.4.0 are incompatible.

With Poetry, if you attempt to install incompatible packages, it will fail before installing any packages.

Alternative package installs
============================

You can directly install from git, so you don’t need to wait for a release to get a new feature you would otherwise be waiting for. The installed commit hash is recorded to `poetry.lock`, so subsequent `poetry install –sync` calls installs the same code.

For example, you can run `poetry add git+https://git@gitlab.com:vmware/idem/idem-aws.git` to install the latest `idem-aws` release.

Learn more
==========

With Poetry, you can quickly install and update package versions. See :ref:`poetry-commands` for more information.
